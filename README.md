# FuzzyClock

A human-readable clock for the gnome-shell panel that indicates the nearest five minute increments
within the hour and splits months into five periods: beginning, early, middle, late and end.

![What time is it?](fuzzy_clock.png)

## Manual Installation

```
cd ~/.local/share/gnome-shell/extensions
```
Download latest [release](https://gitlab.com/Fire-man-x/FuzzyClock/-/releases) and unpack it folder.

Enable or disable the fuzzy clock [via the browser](https://extensions.gnome.org/local/). You may
need to change the `shell-version` field in `metadata.json`. The major version number you are
running can be obtained by running `gnome-shell --version`.

## Development

```
env GNOME_SHELL_SLOWDOWN_FACTOR=2 MUTTER_DEBUG_DUMMY_MODE_SPECS=1024x768 dbus-run-session -- gnome-shell --nested --wayland
```
or
```
dbus-run-session -- gnome-shell --nested --wayland
```

### Debugging
```
journalctl /usr/bin/gnome-shell -f
```

### Translations
Scanning for Translatable Strings

https://gjs.guide/extensions/development/translations.html
```
xgettext --from-code=UTF-8 --output=src/locale/gnome-shell-extension-locale.pot *.js
```

### Packaging
Packing an Extension with Translations
```
cd src
gnome-extensions pack --podir=po
```
or
```
cd src
gnome-extensions pack
```