import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import {Extension, gettext as _, ngettext, pgettext} from 'resource:///org/gnome/shell/extensions/extension.js';

export default class FuzzyClockExtension extends Extension {

	// This creates an object with functions for marking strings as translatable.

	// These are the two most commonly used Gettext functions. The `gettext()`
	// function is often aliased as `_()`
	//ngettext = Extension.ngettext;
	//pgettext = Extension.pgettext;
	//npgettext = Extension.npgettext; // translations with context

	dateMenu = null;
	settings = null;
	fuzzyClock = null;
	updateClockId = 0;

	constructor(metadata) {
		super(metadata);
	}

	//settings set org.gnome.desktop.interface clock-show-seconds true

	/**
	 *
	 * @param GnomeDesktop_WallClock gnomeDesktop_WallClock
	 * @param GObject_ParamSpec gObject_ParamSpec
	 */
	updateClockAndDate(gnomeDesktop_WallClock, gObject_ParamSpec) {
		let tz = this.dateMenu._clock.get_timezone();
		let now = GLib.DateTime.new_now(tz);
		let clockStr = this.fuzzyClock.time(now);
		if (this.settings.get_boolean('clock-show-date')) {
			let dateStr = this.fuzzyClock.date(now);
			this.dateMenu._date.label = dateStr;
			clockStr += ", " + dateStr;
		}
		this.dateMenu._clockDisplay.text = clockStr;
	}

	enable() {
		//this._settings = this.getSettings();
		this.dateMenu = Main.panel.statusArea['dateMenu'];
		if (!this.dateMenu) {
			throw new Error("No dateMenu status area in environment");
		}
		this.settings = new Gio.Settings({schema: 'org.gnome.desktop.interface'});
		this.fuzzyClock = new FuzzyClock();
		if (this.updateClockId !== 0) {
			this.dateMenu._clock.disconnect(this.updateClockId);
		}
		this.updateClockId = this.dateMenu._clock.connect('notify::clock', this.updateClockAndDate.bind(this));
		this.updateClockAndDate();
	}

	disable() {
		if (this.updateClockId !== 0) {
			this.dateMenu._clock.disconnect(this.updateClockId);
			this.updateClockId = 0;
		}
		this.dateMenu._clockDisplay.text = this.dateMenu._clock.clock;
		this.fuzzyClock = null;
		this.settings = null;
		this.dateMenu = null;
	}

}

class FuzzyClock {

	constructor() {
		this.month_format = [
			_("Beginning of %0"),
			_("Early %0"),
			_("Middle of %0"),
			_("Late %0"),
			_("End of %0")
		];
		this.month_names = [
			_("January"),
			_("February"),
			_("March"),
			_("April"),
			_("May"),
			_("June"),
			_("July"),
			_("August"),
			_("September"),
			_("October"),
			_("November"),
			_("December")
		];
	}

	/**
	 * @param hour int
	 * @param minute int
	 * @returns {string}
	 */
	formatHour(hour, minute) {

		let minutesFormat = {
			0: ngettext("X:00", "X:00", hour >= 12 ? hour - 12 : hour), //%is o'clock
			5: _("X:05"), //Five past %ActualHour
			10:_("X:10"), //Ten past %ActualHour
			15:_("X:15"), //Quarter past %ActualHour
			20:_("X:20"), //Twenty past %ActualHour
			25:_("X:25"), //Twenty five past %ActualHour
			30:_("X:30"), //Half past %ActualHour
			35:_("X:35"), //Twenty five to %NextHour
			40:_("X:40"), //Twenty to %NextHour
			45:_("X:45"), //Quarter to %NextHour
			50:_("X:50"), //Ten to %NextHour
			55:_("X:55"), //Five to %NextHour
			60:ngettext("X:60", "X:60", hour >= 12 ? hour - 12 : hour) //%will o'clock
		};

		return minutesFormat[minute];
	};
	/**
	 * @param hour int
	 * @param context string
	 * @returns {string}
	 */
	formatHourName(hour, context) {
		//list for translation programs
		pgettext("ActualHour", "Zero");
		pgettext("actualHour", "Zero");
		pgettext("NextHour", "Zero");
		pgettext("nextHour", "Zero");
		pgettext("ActualHour", "One");
		pgettext("actualHour", "One");
		pgettext("NextHour", "One");
		pgettext("nextHour", "One");
		pgettext("ActualHour", "Two");
		pgettext("actualHour", "Two");
		pgettext("NextHour", "Two");
		pgettext("nextHour", "Two");
		pgettext("ActualHour", "Three");
		pgettext("actualHour", "Three");
		pgettext("NextHour", "Three");
		pgettext("nextHour", "Three");
		pgettext("ActualHour", "Four");
		pgettext("actualHour", "Four");
		pgettext("NextHour", "Four");
		pgettext("nextHour", "Four");
		pgettext("ActualHour", "Five");
		pgettext("actualHour", "Five");
		pgettext("NextHour", "Five");
		pgettext("nextHour", "Five");
		pgettext("ActualHour", "Six");
		pgettext("actualHour", "Six");
		pgettext("NextHour", "Six");
		pgettext("nextHour", "Six");
		pgettext("ActualHour", "Seven");
		pgettext("actualHour", "Seven");
		pgettext("NextHour", "Seven");
		pgettext("nextHour", "Seven");
		pgettext("ActualHour", "Eight");
		pgettext("actualHour", "Eight");
		pgettext("NextHour", "Eight");
		pgettext("nextHour", "Eight");
		pgettext("ActualHour", "Nine");
		pgettext("actualHour", "Nine");
		pgettext("NextHour", "Nine");
		pgettext("nextHour", "Nine");
		pgettext("ActualHour", "Ten");
		pgettext("actualHour", "Ten");
		pgettext("NextHour", "Ten");
		pgettext("nextHour", "Ten");
		pgettext("ActualHour", "Eleven");
		pgettext("actualHour", "Eleven");
		pgettext("NextHour", "Eleven");
		pgettext("nextHour", "Eleven");
		pgettext("ActualHour", "Twelve");
		pgettext("actualHour", "Twelve");
		pgettext("NextHour", "Twelve");
		pgettext("nextHour", "Twelve");

		let hourNames = {
			0:pgettext(context, "Zero"), // translated to Twelve
			1:pgettext(context, "One"),
			2:pgettext(context, "Two"),
			3:pgettext(context, "Three"),
			4:pgettext(context, "Four"),
			5:pgettext(context, "Five"),
			6:pgettext(context, "Six"),
			7:pgettext(context, "Seven"),
			8:pgettext(context, "Eight"),
			9:pgettext(context, "Nine"),
			10:pgettext(context, "Ten"),
			11:pgettext(context, "Eleven"),
			12:pgettext(context, "Twelve")
		};

		return hourNames[hour];
	};


	/**
	 * @param now GLib.DateTime
	 * @returns {string}
	 */
	time(now) {
		let hours = now.get_hour();
		let minutes = Math.round(now.get_minute() / 5) * 5;
		var returnText = this.formatHour(hours, minutes);
		let regExp = /%[aA]ctualHour(:[a-zA-Z]*)?/;
		let matches = returnText.match(regExp);
		if(matches)
		{
			returnText = returnText.replace(regExp, this.formatHourName(hours >= 12 ? hours - 12 : hours, matches[0].substring(1)));
		}
		regExp = /%[nN]extHour(:[a-zA-Z]*)?/;
		matches = returnText.match(regExp);
		if(matches)
		{
			returnText = returnText.replace(regExp, this.formatHourName(hours + 1 >= 12 ? hours + 1 - 12 : hours + 1, matches[0].substring(1)));
		}
		//returnText += " " + now.get_minute();

		return returnText;
	};

	/**
	 * @param now GLib.DateTime
	 * @returns {string}
	 */
	date(now) {
		let month = now.get_month();
		let day = now.get_day_of_month();
		let days = GLib.Date.get_days_in_month(month, now.get_year());
		return this.month_format[Math.round(4 * (day / days))]
			.replace("%0", this.month_names[month - 1]);
	};
}